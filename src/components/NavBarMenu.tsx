import React, { useEffect, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { nav ,text} from "../styles";

import menu from "../assets/menu.png";
import { doLogout, isAuthenticated } from "../services/auth";

const NavBarMenu: React.FC = () => {
    const [show, setShow] = useState(false);
    const navigation = useNavigation();
    const route = useRoute();
    const [authentication, setAuthentication] = useState<boolean>(false);

    function navigate(path: any) {
        if (path) {
            setShow(false);
            navigation.navigate(path);
        }
        setShow(false);
    }
    async function logged() {
        const result = await isAuthenticated();
        result ? setAuthentication(true) : setAuthentication(false)
    }
    function logout(){
        doLogout();
        navigation.navigate("Login");
    }
    useEffect(() => {
        logged();
    }, [])

    return (
        <>
                <TouchableOpacity 
                    activeOpacity={0.8}
                    style={nav.drawerLeft}
                    onPress={() => setShow(!show)}
                >
                    <Image source={menu} style={{}} />
                    {show ? (
                        <View style={nav.options}>
                            <TouchableOpacity style={nav.option} onPress={() => navigate("Home")}>
                                <Text
                                    style={[
                                        nav.textOption,
                                        route.name === "Home" ? nav.textActive : null,
                                    ]}
                                >
                                    Home
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={nav.option}
                                onPress={() => navigate("Catalogo")}
                            >
                                <Text
                                    style={[
                                        nav.textOption,
                                        route.name === "Catalogo" ? nav.textActive : null,
                                    ]}
                                >
                                    Catálogo
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={nav.option}>
                                <Text
                                    style={[
                                        nav.textOption,
                                        route.name === "ADM" ? nav.textActive : null,
                                    ]}
                                    onPress={() => authentication? navigate("Dashboard") :navigate("Login")}
                                >
                                    ADM
                                </Text>
                            </TouchableOpacity>
                        </View>
                    ) : null}
                </TouchableOpacity>
        </>

    );
};

export default NavBarMenu;