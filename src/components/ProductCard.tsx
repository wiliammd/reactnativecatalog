import { ImageSourcePropType, Text, View, TouchableOpacity, Image } from "react-native";
import {useNavigation} from "@react-navigation/native"
import React from 'react';
import { theme , text, admin} from "../styles";
import { TextInputMask } from "react-native-masked-text";
interface ProductProps {
    id: Number;
    name: String;
    imgUrl: string;
    price: string
    role?:String;
    handleDelete:Function;
    handleEdit:Function;
}
const ProductCard: React.FC<ProductProps> = ({ id, name, imgUrl, price ,role,handleDelete,handleEdit}) => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity style={theme.productCard} onPress={()=> role? "": navigation.navigate(`ProductDetails` as never , {id} as never)}>
            <Image source={{uri:imgUrl}} style={theme.imagesSize}/>
            <View style={theme.productDescription}>
                <Text style={text.productName} >{name}</Text>
                <View style={theme.priceContainer}>
                    <Text style={text.currency}>R$</Text>
                    <TextInputMask type={"money"} options={{precision:2, separator:",",delimiter:".",unit:" ",suffixUnit:"",}} value={price} 
                    editable={false} style={text.productPrice}/> 
                    {/* <Text style={text.productPrice}> {price}</Text> */}
                </View>
                { role == 'admin' &&(
                    <View style={admin.buttonContainer}>
                        <TouchableOpacity style={admin.btnEditar} onPress={()=>handleEdit(id)}> 
                            <Text  style={admin.textEditar}>
                                Editar
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={admin.btnExcluir} onPress={()=> handleDelete(id)}>
                            <Text style={admin.textExcluir}>
                                Excluir
                            </Text>
                        </TouchableOpacity>
                    </View>
                )}
            </View>
        </TouchableOpacity>
    );
}
export default ProductCard;