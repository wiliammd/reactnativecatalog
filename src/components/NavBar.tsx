import React, { useEffect, useState } from "react";
import { useNavigation, useRoute } from "@react-navigation/native";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { nav ,text} from "../styles";

import menu from "../assets/menu.png";
import { doLogout, isAuthenticated } from "../services/auth";

const NavBar: React.FC = () => {
    const [show, setShow] = useState(false);
    const navigation = useNavigation();
    const route = useRoute();
    const [authentication, setAuthentication] = useState<boolean>(false);

    function navigate(path: any) {
        if (path) {
            setShow(false);
            navigation.navigate(path);
        }
        setShow(false);
    }
    async function logged() {
        const result = await isAuthenticated();
        result ? setAuthentication(true) : setAuthentication(false)
    }
    function logout(){
        doLogout();
        navigation.navigate("Login");
    }
    useEffect(() => {
        logged();
    }, [])

    return (
        <>
            {authentication ? (
                <TouchableOpacity style={nav.logoutBtn} onPress={()=>logout()}>
                    <Text style={text.logoutText}>Sair</Text>
                </TouchableOpacity>
            ) : (
                <TouchableOpacity style={nav.logoutBtn} onPress={()=>logout()}>
                <Text style={text.logoutText}>Entrar</Text>
            </TouchableOpacity>
            )}
        </>

    );
};

export default NavBar;