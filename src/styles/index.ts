import { Dimensions, PixelRatio, StyleSheet } from "react-native";
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const px = PixelRatio.roundToNearestPixel(deviceWidth);
const colors = {
    white: "#FFF",
    lightGray: "#f2f2f2",
    mediumGray: "#9e9e9e",
    darkGray: "#263238",
    black: "#000",
    primary: "#407BEE",
    secondary: "#33569b",
    bluePill: "#407bff61",
    red: "#df5753",
    borderGray: "#e1e1e1",
}
const text = StyleSheet.create({
    uploadText:{
        color:colors.white,
        textTransform:"uppercase",
        fontWeight:"bold",

    },
    fileSize:{
        color:colors.primary,
        fontSize:10,
        fontWeight:"300",
        marginVertical:5,
        padding:2,
    },
    loginTitle:{
        fontSize: 30,
        fontWeight: "400",
        color: colors.darkGray,
        textTransform:"uppercase",
        marginBottom:50
    },
    regular: {
        fontSize: 16,
        fontWeight: "400",
        textAlign: "center",
        color: colors.mediumGray,
    },
    bold: {
        fontSize: 26,
        fontWeight: "bold",
        textAlign: "center",
        marginBottom: 15,
        color: colors.darkGray,
    },
    primaryText: {
        fontSize: 14,
        fontWeight: "bold",
        textTransform: "uppercase",
        color: colors.white,
        marginLeft: 20,

    },
    //Product Card
    productName: {
        fontSize: 16,
        fontWeight: "bold",

    },
    currency: {
        fontSize: 16,
        fontWeight: "400",
        color: colors.mediumGray,
    },
    productPrice: {
        fontSize: 30,
        fontWeight: "bold",
        color: colors.primary,
    },
    //product details
    productDetailsTitle: {
        fontSize: 30,
        fontWeight: "bold",
        marginTop: 10,
        color: colors.darkGray,
    },
    logoutText:{
        color:colors.white,
    },
    goBackText:{
        fontSize: 18,
        fontWeight: "bold",
        textTransform: "uppercase",
        color: colors.black,
        marginLeft: 20,
    },
});
const theme = StyleSheet.create({
    buttonTextContainer:{
        textAlign:"center",
    },
    olho:{

    },
    formInput:{
        width:"100%",
        height:50,
        borderWidth:1,
        borderColor:colors.mediumGray,
        borderRadius:10,
        padding:10,
        marginVertical:15,
    },
    textArea:{
        width:"100%",
        maxWidth:"100%",
        height:200,
        borderWidth:1,
        borderColor:colors.mediumGray,
        borderRadius:10,
        padding:10,
        marginVertical:15,
    },
    selectInput:{ 
        width:"100%",
        height:50,
        borderWidth:1,
        borderColor:colors.mediumGray,
        borderRadius:10,
        padding:10,
        justifyContent:"center",

    },
    uploadBtn:{
        width:"100%",
        height:40,
        backgroundColor:colors.mediumGray,
        borderRadius:5,
        alignItems:"center",
        justifyContent:"center",
    },
    //login page
    loginCard: {
        width: "100%",
        height: "100%",
        backgroundColor: colors.white,
        borderRadius: 20,
        alignItems: "center",
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        justifyContent: "center",
    },
    form:{
        marginVertical:10
    },
    passwordContainer:{
        flexDirection:"row",
        alignItems:"center",
        marginVertical:25,

    },
    textInput:{
        width:290,
        height:50,
        borderWidth:1,
        borderColor:colors.mediumGray,
        borderRadius:10,
        padding:10
    },
    toggle:{
        margin:-45
    },
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 20

    },
    card: {
        width: "100%",
        height: "100%",
        backgroundColor: colors.white,
        borderRadius: 20,
        alignItems: "center",
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        justifyContent: "space-around",
    },
    image: {
        width: 313,
        height: 225,
    },
    textContainer: {
        paddingHorizontal: 20,
    },
    primaryButton: {
        backgroundColor: colors.primary,
        borderRadius: 10,
        width: 290,
        height: 50,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    
    setaContainer: {
        backgroundColor: colors.secondary,
        height: 50,
        width: 50,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    //scrolConfig
    scrollContainer: {
        paddingHorizontal:10,
    },
    scrollContainerAdminProduct: {
        paddingHorizontal: 10,
        marginTop:-5,
    },
    //Product Card
    productCard: {
        width: "100%",
        backgroundColor: colors.white,
        borderRadius: 10,
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        alignItems: "center",
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginVertical: 10,
    },
    productDescription: {
        width: "100%",
        padding: 20,
        borderTopColor: colors.lightGray,
        borderTopWidth: 1,

    },
    priceContainer: {
        flexDirection: "row",
        marginTop: 10,

    },
    priceContainer2: {
        flexDirection: "row",
        marginTop: 10,
        justifyContent: "flex-start",

    },
    //search input
    inputContainer: {
        width: "100%",
        height: 60,
        backgroundColor: colors.white,
        borderRadius: 10,
        alignItems: "center",
        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        marginVertical: 12.5,
        paddingVertical: 10,
    },
    searchInput: {
        width: "90%",
        height: 40,
        borderBottomWidth: 0.5,
        borderBottomColor: colors.borderGray
    },
    imagesSize: {
        width: 140,
        height: 140,
        margin: 16,
    },
    //product details
    detailsContainer: {
        padding: 20,
    },
    detailCard: {
        width: "100%",
        height: "100%",
        backgroundColor: colors.white,
        borderRadius: 20,

        shadowColor: colors.black,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        justifyContent: "space-around",
        padding: 20,

    },
    productImageContainer: {
        width: '100%',
        borderWidth: 1,
        borderColor: colors.lightGray,
        alignItems: "center",
        borderRadius: 20,

    },
    goBackContainer: {
        width: 290,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-start",
        marginVertical: 10,

    },
    scrollTextContainer: {
        marginVertical: 20,
        padding: 20,
        borderWidth: 0.2,
        borderRadius: 10,

    },
    productDescriptionText: {
        fontSize: 16,
        fontWeight: "400",
        color: colors.mediumGray,
    },
    //nao esta sendo usada
    arrow: {
        transform: [{ rotate: "180deg" }],
    }

});
const nav = StyleSheet.create({
    leftText: {
        color: colors.white,
        fontWeight: "bold",
        marginLeft: 15,
    },
    drawer: {
        marginRight: 15,
        padding: 0
    },
    drawerLeft: {
        marginLeft: 15,
        padding: 0
    },
    options: {
        width: deviceWidth,
        marginTop: 146,
        marginLeft: -15,
        height: 120,
        backgroundColor: colors.primary,

        
        padding: 15,
        justifyContent: "space-between"
    },
    option: {
        padding: 1,
        marginLeft: 15,
    },
    textOption: {
        color: colors.white,
        textTransform: "uppercase",
    },
    textActive: {
        fontWeight: "bold",
    },
    logoutBtn:{
        width:60,
        height:30,
        borderWidth:1,
        borderColor:colors.white,
        borderRadius:10,
        alignItems:"center",
        justifyContent:"center",
        marginRight:15
    },

});
const tabbar = StyleSheet.create({
    container:{
        width:deviceWidth,
        height:80,
        backgroundColor:colors.white,
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-around",
    },
    pill:{
        padding:15,
        backgroundColor:colors.lightGray,
        borderRadius:20,
    },
    pillActive:{
        backgroundColor:colors.bluePill,
    },
    pillText:{
        fontWeight:"bold",
        color:colors.mediumGray,
    },
    pillTextActive:{
        color:colors.primary,
    },
});
const admin = StyleSheet.create({
    primaryButtonAdicionar: {
        backgroundColor: colors.primary,
        borderRadius: 10,
        width: "100%",
        height: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    buttonContainer:{
        flexDirection:"row",
        alignItems:'center',
        justifyContent:'space-around',
        marginTop:10,

    },
    btnExcluir: {
        borderColor: colors.red,
        borderRadius: 10,
        borderWidth:1,
        width: "48%",
        height: 40,
        justifyContent: "center",
        alignItems: "center",
    },
    btnEditar:{
        borderColor: colors.mediumGray,
        borderRadius: 10,
        borderWidth:1,
        width: "48%",
        height: 40,
        justifyContent: "center",
        alignItems: "center",
    },
    btnSalvar:{
        backgroundColor: colors.primary,
        borderRadius: 10,
        width: "48%",
        height: 40,
        justifyContent: "center",
        alignItems: "center",
    },
    textExcluir:{
        textTransform:"uppercase",
        fontWeight:"bold",
        color:colors.red,
    },
    textEditar:{
        textTransform:"uppercase",
        fontWeight:"bold",
        color:colors.mediumGray,
    },
    textSalvar:{
        textTransform:"uppercase",
        fontWeight:"bold",
        color:colors.white,
    },
})
const formProduct = StyleSheet.create({
    formContainer:{
        width:deviceWidth,
        padding:20,
    },
    formCard:{
        width:"100̈́%",
        height:"92%",
        backgroundColor:colors.white,
        borderRadius:20,
        padding:20,
        shadowColor:colors.black,
        shadowOffset:{
            width:0,
            height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        alignItems:"center",
        justifyContent:"space-around",
    },
    modalContainer:{
        width:deviceWidth,
        height:deviceHeight,
        backgroundColor:"#00000033",
        alignItems:"center",
        justifyContent:"center",
    },
    modalContent:{
        width:300,
        justifyContent:"center",
        alignItems:"center",
        marginTop:"50%",
        backgroundColor:colors.white,
        borderRadius:20,
        padding:20,
        shadowColor:colors.black,
        shadowOffset:{
            width:0,
            height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:5,
    },
    modalItem:{
        width:"100%",
        backgroundColor:colors.lightGray,
        padding:10,
        marginVertical:5,
        borderRadius:5,
    },
    goBackContainer:{

    },
    

})
export { colors, theme, text, nav, tabbar, admin, formProduct };
