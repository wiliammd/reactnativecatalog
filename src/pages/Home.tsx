import { Text, TouchableOpacity, View, StyleSheet, Image } from "react-native";
import React from 'react';
import { colors, text, theme } from "../styles";
import seta from '../assets/seta.png';
import imagem from '../assets/imagem.png';
import { useNavigation } from "@react-navigation/native";

const Home: React.FC = () => {
    const navigation = useNavigation();
    return (
        <View style={theme.container}>
            <View style={theme.card}>
                <Image source={imagem} style={theme.image} />
                <View style={theme.textContainer}>
                    <Text style={text.bold}>Conheça o melhor catálogo de produtos</Text>
                    <Text style={text.regular}>Ajudaremos você a encontrar os melhores produtos disponíveis no mercado.</Text>
                </View>
                <TouchableOpacity style={theme.primaryButton} activeOpacity={0.8} onPress={()=> navigation.navigate("Catalogo")}>
                    <Text style={text.primaryText}>Inicie agora sua Busca</Text>
                    <View style={theme.setaContainer}>
                        <Image source={seta} />
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        backgroundColor: "#61dafb"
    },
    button: {


        backgroundColor: '#069',
        padding: 10,
        borderRadius: 4
    }
});
export default Home;