import React, { useEffect, useState } from 'react';
import { View,ActivityIndicator, Text, TouchableOpacity, Image, ScrollView } from 'react-native';
import { api, getProduct } from '../services';
import {colors, text, theme} from "../styles";
import arrow from '../assets/Arrow.png';
import { useNavigation } from '@react-navigation/native';

const ProductDetails = ({route:{params:{id}}}) => {
    const [product, setProduct] = useState({id:null,name:null,description:null,price:null,imgUrl:null,date:null,categories:[]});
    const [loading,setLoading] = useState(false);

    async function loadProductData(){
        setLoading(true);
        const res = await getProduct(id);
        console.log(res);
        setProduct(res.data);
        setLoading(false);
    }
    useEffect(()=>{
        loadProductData()
    },[])
    const navigation = useNavigation();
    return (
        <View style={theme.detailsContainer}>
            {loading?(<ActivityIndicator size="large"/>): 
               ( <View style={theme.detailCard}>
                   <TouchableOpacity style={theme.goBackContainer} onPress={()=> navigation.goBack()}>
                       <Image source={arrow} style={{marginEnd:10}}/>
                       <Text style={{color:colors.primary, fontSize:18,textTransform:"uppercase"}}>Voltar</Text>
                   </TouchableOpacity>
                   <View style={theme.productImageContainer}>
                       <Image source={{uri:product.imgUrl}} style={{width:150,height:150}}/>
                   </View>
                   <Text style={text.productDetailsTitle}>{product.name}</Text>
                   <View style={theme.priceContainer2}>
                       <Text style={text.currency}>
                           R$
                       </Text>
                       <Text style={text.productPrice}>{product.price}</Text>
                    </View>
                    <ScrollView style={theme.scrollTextContainer}>
                        <Text style={theme.productDescriptionText}>{product.description}</Text>
                    </ScrollView>

                </View> )}
        </View>
    )
}
export default ProductDetails;