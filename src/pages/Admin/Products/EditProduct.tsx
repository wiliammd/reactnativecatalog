import * as ImagePicker from "expo-image-picker";
import React, { useEffect, useState } from "react";
import { ActivityIndicator, Alert, Image, Modal, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import Toast from "react-native-tiny-toast";
import Arrow from '../../../assets/Arrow.png';
import { getCategories, getProduct, updateProduct, uploadImage } from "../../../services";
import { admin, formProduct, text, theme } from "../../../styles";
interface FormProductProps {
    setScreen: Function;
    productId: number;
}

const EditProduct: React.FC<FormProductProps> = (props) => {
    const [loading, setLoading] = useState(false);
    const [categories, setCategories] = useState([])
    const [showCategories, setShowCategories] = useState(false);
    const [product, setProduct] = useState({ name: "", description: "", imgUrl: " ", price: "", categories: [] });
    const { setScreen, productId } = props;
    const [image, setImage] = useState("");

    async function loadCategories() {
        setLoading(true);
        const res = await getCategories();
        setCategories(res.data.content);
        setLoading(false);
    }

    useEffect(() => {
        async () => {
            const { status } = await ImagePicker.requestCameraPermissionsAsync();
            if (status != 'granted') {
                Alert.alert("Precisamos de aceso a biblioteca de imagens!");
            }
        }
    }, [])
    async function loadProduct() {
        setLoading(true);
        const res = await getProduct(productId);
        setProduct(res.data);
        setLoading(false);

    }
    useEffect(() => {
        image ? handleUpload() : null;
    }, [image])
    useEffect(() => {
        loadCategories();
        loadProduct();
    }, [])

    const selectImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        !result.cancelled && setImage(result.uri);
    }
    function getRow(e) {
        const str = e;
        const res = str.slice(2).replace(/\./g, "").replace(/,/g, ".");
        return res;
    }
    async function handleUpload() {
        uploadImage(image).then(res => {
            const { uri } = res?.data;
            setProduct({ ...product, imgUrl: uri });
        });
    }


    const handleSave = async () => {
        setLoading(true);
        const data = {
            ...product,
        }
        try {
            await updateProduct(data);
            setScreen("products");
            Toast.showSuccess("Produto Editado com sucesso!")
        } catch (error) {
            Toast.show("erro ao salvar", error);
        }
        setLoading(false);
    }
    return (
        <View style={formProduct.formContainer}>
            {
                loading ? (<ActivityIndicator size="large" />) : (
                    <ScrollView contentContainerStyle={formProduct.formCard}>
                        <ScrollView>


                            <Modal visible={showCategories} animationType="fade" transparent={true} presentationStyle="overFullScreen" >
                                <View style={formProduct.modalContainer}>
                                    <ScrollView contentContainerStyle={formProduct.modalContent}>
                                        {categories.map((cat) => {
                                            const { id, name } = cat;
                                            return (
                                                <TouchableOpacity style={formProduct.modalItem} key={id} onPress={() => { setProduct({ ...product, categories: [{ id, name }] }); setShowCategories(!showCategories); }}>
                                                    <Text>{name}</Text>
                                                </TouchableOpacity>
                                            )
                                        })}
                                    </ScrollView>
                                </View>
                            </Modal>
                            <TouchableOpacity onPress={() => setScreen("products")} style={theme.goBackContainer}>
                                <Image source={Arrow} />
                                <Text style={text.goBackText}>
                                    Voltar
                                </Text>
                            </TouchableOpacity>
                            <TextInput placeholder="Nome do produto" style={theme.formInput} value={product.name} onChangeText={(e) => setProduct({ ...product, name: e })} />
                            <TouchableOpacity onPress={() => setShowCategories(!showCategories)} style={theme.selectInput}>
                                <Text style={product.categories.length == 0 && { color: "#cecece" }}>
                                    {
                                        product.categories.length == 0 ? 'Escolha uma categoria' : product.categories[0].name
                                    }
                                </Text>
                            </TouchableOpacity>
                            <TextInputMask type={"money"} placeholder="Preço" style={theme.formInput} value={product.price} onChangeText={(e) => setProduct({ ...product, price: getRow(e) })} />
                            {/* <TextInput placeholder="Preço" style={theme.formInput} keyboardType="numeric" value={product.price.toString()} onChangeText={(e)=> setProduct({...product, price:parseInt(e)})}/> */}
                            <TouchableOpacity activeOpacity={0.8} style={theme.uploadBtn} onPress={selectImage}>
                                <Text style={text.uploadText}>Carregar Imagem</Text>
                            </TouchableOpacity>
                            <Text style={text.fileSize} >
                                As imagens devem ser  JPG ou PNG e não devem ultrapassar 5 mb.
                            </Text>
                            <Text>
                                {product.categories.length > 0 && product.categories[0].name}
                            </Text>
                                    <TouchableOpacity onPress={selectImage} activeOpacity={0.8} style={{ width: "100%", height: 150, borderRadius: 10, marginVertical: 10, }}>
                                        <Image source={image == ""?{ uri: product.imgUrl }:{ uri: image }} style={{ width: "100%", height: "100%", borderRadius: 10 }} />
                                    </TouchableOpacity>
                            <TextInput multiline placeholder="Descrição" style={theme.textArea} value={product.description} onChangeText={(e) => setProduct({ ...product, description: e })} />
                            <View style={admin.buttonContainer}>
                                <TouchableOpacity style={admin.btnExcluir}
                                    onPress={() => {
                                        Alert.alert("Deseja cancelar?", "Os dados inseridos não serão salvos!",
                                            [
                                                { text: "Voltar", style: "cancel" },
                                                { text: "Confirmar", onPress: () => setScreen("products"), style: "default", },
                                            ]
                                        )
                                    }
                                    }>
                                    <Text style={admin.textExcluir}>Cancelar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={admin.btnSalvar} onPress={handleSave}>
                                    <Text style={admin.textSalvar}>Salvar</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </ScrollView>
                )
            }

        </View>
    )
};

export default EditProduct;