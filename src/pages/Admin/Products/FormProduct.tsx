import React, { useState, useEffect } from "react";
import { Text, View, ScrollView, TouchableOpacity, Image, Modal, TextInput, ActivityIndicator, Alert } from "react-native";
import { TextInputMask } from "react-native-masked-text";
import Toast from "react-native-tiny-toast";
import Arrow from '../../../assets/Arrow.png';
import { createProduct, getCategories, uploadImage } from "../../../services";
import { admin, formProduct, text, theme } from "../../../styles";
import * as ImagePicker from "expo-image-picker";
interface FormProductProps {
    setScreen: Function;
}

const FormProduct: React.FC<FormProductProps> = (props) => {
    const [loading, setLoading] = useState(false);
    const [categories, setCategories] = useState([])
    const [showCategories, setShowCategories] = useState(false);
    const [product, setProduct] = useState({ name: "", description: "", imgUrl: "", price: "", categories: [] });
    const { setScreen } = props;
    const [image, setImage] = useState("");

    async function loadCategories() {
        setLoading(true);
        const res = await getCategories();
        setCategories(res.data.content);
        setLoading(false);
    }

    useEffect(() => {
        async () => {
            const { status } = await ImagePicker.requestCameraPermissionsAsync();
            if (status != 'granted') {
                Alert.alert("Precisamos de aceso a biblioteca de imagens!");
            }
        }
    }, [])

    const selectImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        !result.cancelled &&setImage(result.uri);
    }

    async function handleUpload() {
        uploadImage(image).then(res => {
            const { uri } = res?.data;
            console.log(res);
            setProduct({ ...product, imgUrl: uri });
        });
    }
    useEffect(() => {
        image ? handleUpload() : null;
    }, [image])
    useEffect(() => {
        loadCategories();
    }, [])
    function handleSave() {
        newProduct();
    }
    function getRow() {
        const str = product.price;
        const res = str.slice(2).replace(/\./g, "").replace(/,/g, ".");
        return res;
    }

    const newProduct = async () => {
        setLoading(true);
        const cat = replaceCategory();
        const data = {
            ...product,
            price: getRow(),
            categories: [
                {
                    id: cat,
                }
            ]
        }
        try {
            await createProduct(data);
            Toast.showSuccess("Produto criado com sucesso!")
        } catch (error) {
            Toast.show("erro ao salvar", error);
        }
        setLoading(false);
    }
    const replaceCategory = () => {
        const cat = categories.find(category => category.name == product.categories);
        return cat.id;
    }
    return (
        <View style={formProduct.formContainer}>
            {
                loading ? (<ActivityIndicator size="large" />) : (
                    <ScrollView contentContainerStyle={formProduct.formCard}>
                        <ScrollView>


                            <Modal visible={showCategories} animationType="fade" transparent={true} presentationStyle="overFullScreen" >
                                <View style={formProduct.modalContainer}>
                                    <ScrollView contentContainerStyle={formProduct.modalContent}>
                                        {categories.map((cat) => (
                                            <TouchableOpacity style={formProduct.modalItem} key={cat.id} onPress={() => { setProduct({ ...product, categories: cat.name }); setShowCategories(!showCategories); }}>
                                                <Text>{cat.name}</Text>
                                            </TouchableOpacity>
                                        ))}
                                    </ScrollView>
                                </View>
                            </Modal>
                            <TouchableOpacity onPress={() => setScreen("products")} style={theme.goBackContainer}>
                                <Image source={Arrow} />
                                <Text style={text.goBackText}>
                                    Voltar
                                </Text>
                            </TouchableOpacity>
                            <TextInput placeholder="Nome do produto" style={theme.formInput} value={product.name} onChangeText={(e) => setProduct({ ...product, name: e })} />
                            <TouchableOpacity onPress={() => setShowCategories(!showCategories)} style={theme.selectInput}>
                                <Text style={product.categories.length == 0 && { color: "#cecece" }}>
                                    {
                                        product.categories.length == 0 ? 'Escolha uma categoria' : product.categories
                                    }
                                </Text>
                            </TouchableOpacity>
                            <TextInputMask type={"money"} placeholder="Preço" style={theme.formInput} value={product.price} onChangeText={(e) => setProduct({ ...product, price: e })} />
                            {/* <TextInput placeholder="Preço" style={theme.formInput} keyboardType="numeric" value={product.price.toString()} onChangeText={(e)=> setProduct({...product, price:parseInt(e)})}/> */}
                            <TouchableOpacity activeOpacity={0.8} style={theme.uploadBtn} onPress={selectImage}>
                                <Text style={text.uploadText}>Carregar Imagem</Text>
                            </TouchableOpacity>
                            <Text style={text.fileSize} >
                                As imagens devem ser  JPG ou PNG e não devem ultrapassar 5 mb.
                            </Text>
                            {
                                image != "" && (
                                    <TouchableOpacity onPress={selectImage} activeOpacity={0.8} style={{ width: "100%", height: 150, borderRadius: 10, marginVertical: 10, }}>
                                        <Image source={{ uri: image }} style={{ width: "100%", height: "100%", borderRadius: 10 }} />
                                    </TouchableOpacity>)
                            }
                            <TextInput multiline placeholder="Descrição" style={theme.textArea} value={product.description} onChangeText={(e) => setProduct({ ...product, description: e })} />
                            <View style={admin.buttonContainer}>
                                <TouchableOpacity style={admin.btnExcluir}
                                    onPress={() => {
                                        Alert.alert("Deseja cancelar?", "Os dados inseridos não serão salvos!",
                                            [
                                                { text: "Voltar", style: "cancel" },
                                                { text: "Confirmar", onPress: () => setScreen("products"), style: "default", },
                                            ]
                                        )
                                    }
                                    }>
                                    <Text style={admin.textExcluir}>Cancelar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={admin.btnSalvar} onPress={() => handleSave()}>
                                    <Text style={admin.textSalvar}>Salvar</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </ScrollView>
                )
            }

        </View>
    )
};

export default FormProduct;