import React,{useEffect, useState} from "react";
import { Text, View,TouchableOpacity,ActivityIndicator, FlatList } from "react-native";
import Toast from "react-native-tiny-toast";
import {SearchInput,ProductCard} from "../../../components";
import { deleteProduct, getProducts } from "../../../services";
import { text, theme,admin } from "../../../styles";

interface ProductProps {
    setScreen:Function;
    setProductId:Function;
}

const ListProducts: React.FC<ProductProps> = (props) =>{
    const [search,setSearch] = useState("");
    const [products,setProducts] = useState([]);
    const [loanding,setLoanding] = useState(false);
    const [page,setPage] = useState(0);
    const [totalPage,setTotalPage] = useState(1);
    const perPage = 10;

    const {setScreen,setProductId} = props;
    async function atualizaLista(){
        const res = await getProducts(page,perPage);
        setProducts([...res.data.content]);
    }
    function handleEdit(id:number){
        setProductId(id);
        setScreen("editProduct");
    }

    //function
    async function fillListProducts(){
        if(loanding)return;

        if(totalPage>page){
            setLoanding(true);
            const res = await getProducts(page,perPage);
            setProducts([...products,...res.data.content]);
            setTotalPage(res.data.size -1)
            setPage(page+1);
            setLoanding(false);
        }else{
            return null;
        }
    }
    //arrow function
    const handleDelete = async (id:number) =>{
        setLoanding(true);
        try {
            const res = await deleteProduct(id);
            Toast.showSuccess(`Produto de id:${id} excluido com sucesso!`);
            setLoanding(false);
            await atualizaLista();
        } catch (error) {
            Toast.show("Erro ao excluir produto!",error);
        }finally{
            setLoanding(false);
        }
    }
    useEffect(()=>{
        fillListProducts();
    },[])
    const data = search.length> 0 ? products.filter(product => product.name.toLowerCase().includes(search.toLocaleLowerCase())) : products;
    return (
        <>
        
        <View style={theme.scrollContainerAdminProduct}>
        <TouchableOpacity style={admin.primaryButtonAdicionar} onPress={() => setScreen("newProduct")}>
            <Text style={text.primaryText}>Adicionar</Text>
        </TouchableOpacity>
        <SearchInput search={search} setSearch={setSearch} placeholder="Nome do produto"/>
        </View>
        <FlatList contentContainerStyle={theme.scrollContainer} 
            data={data} keyExtractor={item=> item.id.toString()} onEndReached={fillListProducts} onEndReachedThreshold={0.1} ListFooterComponent={<FooterList load={loanding}/>}
            renderItem={({item})=><ProductCard {...item} key={item.id} role={"admin"} handleDelete={handleDelete} handleEdit={handleEdit}/>}>
        
        </FlatList>
        </>
    );
    function FooterList({load}){
        if(!load)return (
            <View>
                <Text  style={{padding:80}}/>
            </View>
        );
        return(
            <View>
                <ActivityIndicator size="large" color="#121212" style={{padding:10}}/>
            </View>
        );
    
    }
}
export default ListProducts;