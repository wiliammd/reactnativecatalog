import { ActivityIndicator, FlatList, ImageSourcePropType, ScrollView, Text, View } from "react-native";
import React, {useState,useEffect} from 'react';
import {ProductCard, SearchInput} from "../components";


import { theme } from "../styles";
import { api } from "../services";



const Catalog: React.FC = () =>{
    const [search,setSearch] = useState("");
    const [products,setProducts] = useState([]);
    const [loanding,setLoanding] = useState(false);
    const [page,setPage] = useState(0);
    const [totalPage,setTotalPage] = useState(1);
    const perPage = 10;

    async function fillProducts(){
        if(loanding)return;

        if(totalPage>page){
            setLoanding(true);
            const res = await api.get(`/products?page=${page}&linesPerPage=${perPage}&direction=ASC&orderBy=name`)
            setProducts([...products,...res.data.content]);
            setTotalPage(res.data.size -1)
            setPage(page+1);
            setLoanding(false);
    }else{
        return null;
}
    }
    useEffect(()=>{
        fillProducts();
    },[])
    const data = search.length> 0 ? products.filter(product => product.name.toLowerCase().includes(search.toLocaleLowerCase())) : products;
    return (
        <>
        <View style={theme.scrollContainer}>
        <SearchInput search={search} setSearch={setSearch} placeholder="Nome do produto"/>
        
        <FlatList contentContainerStyle={theme.scrollContainer} 
            data={data} keyExtractor={item=> item.id.toString()} onEndReached={fillProducts} onEndReachedThreshold={0.1} ListFooterComponent={<FooterList load={loanding}/>}
            renderItem={({item})=><ProductCard {...item} key={item.id}/>}>
        
        </FlatList>
        </View>
        </>
    );
    function FooterList({load}){
        if(!load)return <View style={{marginTop:100}}></View>;
        return(
            <View>
                <ActivityIndicator size={25} color="#121212" style={{padding:10}}/>
            </View>
        );
    
    }
}
export default Catalog;