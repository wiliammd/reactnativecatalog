import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from 'react-native';
import { text, theme } from '../styles';
import olhoaberto from '../assets/olhoaberto.png';
import olhofechado from '../assets/olhofechado.png';
import seta from '../assets/seta.png';
import { isAuthenticated, login } from '../services/auth';
import { useNavigation } from '@react-navigation/native';
const Login: React.FC = () => {
    const navigation = useNavigation();
    const [aberto,setAberto] = useState<boolean>(false);
    const [userInfo,setuserInfo] = useState({username:"",password:""});
    const [userFetchData,setUserFetch] = useState({});
    useEffect(()=>{
        isAuthenticated();
    },[])
    async function handleLogin(){
        const data = await login(userInfo);
        setUserFetch(data);
        navigation.navigate("Dashboard");
    } 
    return (
        <View style={theme.container}>
            <View style={theme.loginCard}>
                <Text style={text.loginTitle}>Login</Text>
                <View style={theme.form}>
                    <TextInput value={userInfo.username} placeholder="Email" autoCapitalize="none" 
                    keyboardType="email-address" style={theme.textInput} onChangeText={(e)=>{const newUserInfo = {...userInfo}; 
                    newUserInfo.username = e; setuserInfo(newUserInfo)}}/>
                    <View style={theme.passwordContainer}>
                        <TextInput value={userInfo.password} placeholder="Senha" autoCapitalize="none" secureTextEntry={!aberto} style={theme.textInput}
                        onChangeText={(e)=>{const newUserInfo = {...userInfo}; 
                        newUserInfo.password = e; setuserInfo(newUserInfo)}} />
                        <TouchableOpacity style={theme.toggle}onPress={()=>setAberto(!aberto)}>
                            <Image style={theme.olho} source={aberto?olhoaberto:olhofechado}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableOpacity style={theme.primaryButton} activeOpacity={0.8} onPress={()=> handleLogin()}>
                    <View style={theme.buttonTextContainer}>
                        <Text style={text.primaryText}>Fazer Login</Text>
                    </View>
                    <View style={theme.setaContainer}>
                        <Image source={seta} />
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
};
export default Login;