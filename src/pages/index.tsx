export {default as Home} from './Home';
export {default as Catalog} from './Catalog';
export {default as ProductDetails} from './ProductDetails';
export { default as Login } from "../pages/Login";
//telas admin
export {default as Dashboard} from "./Admin/Dashboar";